#!/bin/bash

usage()
{
    echo
    echo "Usage: ./export.sh <model_name>"
    echo
    echo "where <model_name> could be one of the following:"
    echo "    ssd_mobilenet_v1_egohands"
    echo "    ssd_mobilenet_v2_egohands"
    echo "    ssdlite_mobilenet_v2_egohands"
    echo "    ssd_inception_v2_egohands"
    echo "    rfcn_resnet101_egohands"
    echo "    faster_rcnn_resnet50_egohands"
    echo "    faster_rcnn_resnet101_egohands"
    echo "    faster_rcnn_inception_v2_egohands"
    echo "    faster_rcnn_inception_resnet_v2_atrous_egohands"
    echo
    exit
}

if [ $# -ne 1 ]; then
    usage
fi

export SAMPLE_DIR=/opt/intel/openvino/deployment_tools/open_model_zoo/demos/python_demos/object_detection_demo_ssd_async

case $1 in
    ssdlite_mobilenet_v2_egohands | \
    ssd_inception_v2_egohands)
        MODEL=ir_model/$1/handmodel.xml
        ;;
    faster_rcnn_inception_v2_egohands)
        MODEL=ir_model/faster_rcnn_inception_v2_egohands/hand_model_frcnn.xml
        ;;
    * )
        usage
esac

python $SAMPLE_DIR/object_detection_demo_ssd_async.py -m $MODEL -i cam -l /home/lattice/inference_engine_samples_build/intel64/Release/lib/libcpu_extension.so -pt 0.8
