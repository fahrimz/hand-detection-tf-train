#!/bin/bash

usage()
{
    echo
    echo "Usage: ./export.sh <model_name>"
    echo
    echo "where <model_name> could be one of the following:"
    echo "    ssd_mobilenet_v1_egohands"
    echo "    ssd_mobilenet_v2_egohands"
    echo "    ssdlite_mobilenet_v2_egohands"
    echo "    ssd_inception_v2_egohands"
    echo "    rfcn_resnet101_egohands"
    echo "    faster_rcnn_resnet50_egohands"
    echo "    faster_rcnn_resnet101_egohands"
    echo "    faster_rcnn_inception_v2_egohands"
    echo "    faster_rcnn_inception_resnet_v2_atrous_egohands"
    echo
    exit
}

if [ $# -ne 1 ]; then
    usage
fi

export OPENVINO=/opt/intel/openvino
export MODEL_OPTIMIZER=$OPENVINO/deployment_tools/model_optimizer
export MODEL_DIR=$(pwd)/model_exported

case $1 in
    ssdlite_mobilenet_v2_egohands | \
    ssd_inception_v2_egohands | \
    ssd_mobilenet_v2_egohands)
        EXTENSION=$MODEL_OPTIMIZER/extensions/front/tf/ssd_v2_support.json
        ;;
    faster_rcnn_inception_v2_egohands)
        EXTENSION=$MODEL_OPTIMIZER/extensions/front/tf/faster_rcnn_support_api_v1.10.json
        ;;
    * )
        usage
esac

python $MODEL_OPTIMIZER/mo_tf.py \
 --tensorflow_use_custom_operations_config $EXTENSION \
 --tensorflow_object_detection_api_pipeline_config $MODEL_DIR/pipeline.config \
 --input_model $MODEL_DIR/frozen_inference_graph.pb \
 --reverse_input_channels
#  --output=detection_boxes,detection_scores,num_detections \
#  --input_shape [1,300,300,3] \